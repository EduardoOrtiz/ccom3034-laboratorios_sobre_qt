#include <QDebug>
#include <QElapsedTimer>
#include <QThread>
#include <QMutex>
#include <ctime>
#include <cstdlib>

// This function performs a useless task but it takes a while
unsigned int someOddLabor(unsigned int seed) {
   unsigned int accum = seed;
   for(int i = 0; i < 10000; i++ ) {
       for(int j = 0; j < 10000; j++ )
           accum = accum * 37 + 53;
   }
   return accum;
}

//sequential
void test00() {
   QElapsedTimer myTimer;
   srand(time(NULL));
   myTimer.start();

  /* unsigned int result1, result2;
   qDebug() << "Running two tasks sequentially...";
   result1 = someOddLabor(25);
   qDebug() << "Result of first call:" << result1;
   result2 = someOddLabor(48);
   qDebug() << "Result of second call:" << result2;*/

   qDebug() << "Running the labor 20 times...";
   unsigned int result = 0, n = 20;
   for(unsigned int c = 0; c < n; c++)
       result = someOddLabor(rand()%50 + 1);

   qDebug() << "The result is " << result;
   qDebug() << "Total time elapsed: "
            << myTimer.elapsed() << "miliseconds";
   qDebug() << "==================================";
}


class MyOwnThread : public QThread {
private:
   int mySeed;
public:
   MyOwnThread() {};
   MyOwnThread(int s) {mySeed=s;};
   virtual void run();
};
void MyOwnThread::run() {
   unsigned int  a = 0;
   qDebug() << "A thread starting with a seed:" << mySeed;
   a = someOddLabor(mySeed);
   qDebug() << "Thread with seed " << mySeed << " finished. Result:" << a;
}

void test01() {
   MyOwnThread *T01, *T02;
   QElapsedTimer myTimer;
   myTimer.start();
   qDebug() << "Running two tasks as parallel threads...";
   // Create the threads
   T01 = new MyOwnThread(25);
   T02 = new MyOwnThread(48);
   // Start them
   T01->start();
   T02->start();
   // Wait for them to finish
   T01->wait();
   T02->wait();
   qDebug() << "Total time elapsed for test01: "
            << myTimer.elapsed() << "miliseconds";
   qDebug() << "==================================";
}

void test02B() {
   QElapsedTimer myTimer;
   myTimer.start();
   MyOwnThread *T[20];

   qDebug() << "Running 20 threads . . ";
   // create 20 threads
   for (int i=0; i<20; i++) T[i] = new MyOwnThread(i);

   // start the 20 threads
   for (int i=0; i<20; i++) T[i]->start();
   // wait for the 20 threads to finish
   for (int i=0; i<20; i++) T[i]->wait();

   /*for(int i = 0; i < 20; i++)
   {
       T[i]->start();
       T[i]->wait();
   }*/

   qDebug() << "Total time elapsed for test02: "
            << myTimer.elapsed() << "miliseconds";
   qDebug() << "================================";
}


#define INC_QTY 10000000
unsigned int sharedCount;   // the shared variable
class MyThreadIncrement : public QThread {
public:
  virtual void run();
};
void MyThreadIncrement::run()
{
  for( int count = 0; count < INC_QTY; count++ ) {
      sharedCount++;
  }
}
void testRaceCondition(){
   QElapsedTimer myTimer;
   MyThreadIncrement a,b;   // we create two threads
   sharedCount = 0;
   myTimer.start();
   a.start(); b.start();    // we run the threads
   a.wait();  b.wait();     // we wait for them to finish
   qDebug() << "Finished both threads, the sharedCount is: " << sharedCount;
   qDebug() << "Total time elapsed for the concurrent execution was : "
            << myTimer.elapsed() << "miliseconds";
   qDebug() << "================================";
}


QMutex mutex;
class MyThreadIncrementSafe : public QThread {
public:
   virtual void run();
};
void MyThreadIncrementSafe ::run() {
   for( int count = 0; count < INC_QTY; count++ ) {
       mutex.lock();       // <-- instrucción añadida
       sharedCount++;
       mutex.unlock();     // <-- instrucción añadida
   }
}
void testMutex(){
    QElapsedTimer myTimer;
    sharedCount = 0;
    MyThreadIncrementSafe  a,b;
    myTimer.start();
    a.start();   b.start();
    a.wait();    b.wait();
    qDebug() << "Finally, the sharedCount is: " << sharedCount << endl;
    qDebug() << "Finished both threads, the sharedCount is: " << sharedCount;
    qDebug() << "Total time elapsed for the concurrent execution was : "
             << myTimer.elapsed() << "miliseconds";
    qDebug() << "================================";
}
void maxSequential(double * arreglo, unsigned int size)
{
     QElapsedTimer myTimer;
     myTimer.start();

     double max = arreglo[0];
     for(unsigned int c = 1; c < size; c++)
         if(max < arreglo[c])
             max = arreglo[c];

     qDebug() << "The max of this array is:" << max;
     qDebug() << "The total time for the Sequential search is:"
              << myTimer.elapsed() << "milisedonds.";
     qDebug() << "================================";
}

double MaxForThread(double * arreglo, unsigned int starting, unsigned int finishing)
{
  double temp_max = arreglo[starting];

  for(unsigned int i = starting; i < finishing; i++)
    if(temp_max < arreglo[i])
      temp_max = arreglo[i];

  return temp_max;
}


unsigned int twoHundredMillion = 200000000;
double * array = new double[twoHundredMillion];

class MaxThread : public QThread
{
    public:
    unsigned int starting_point;
    unsigned int finish;
    double TMax;
    MaxThread(unsigned int s, unsigned int f)
    {starting_point = s; finish = f;}
    virtual void run();
};

void MaxThread::run()
{
  this->TMax = MaxForThread(array, starting_point, finish);
}

void MaxParallel()
{
  QElapsedTimer myTimer;
  MaxThread  *T1, *T2;
  T1 = new MaxThread(0, twoHundredMillion/2);
  T2 = new MaxThread(twoHundredMillion/2, twoHundredMillion);
  double true_max;
  myTimer.start();

  T1->start(); T2->start();
  T1->wait(); T2->wait();

  if(T1->TMax > T2->TMax)
    true_max = T1->TMax;
  else
    true_max = T2->TMax;

  qDebug() << "The max of this array is:" << true_max;
  qDebug() << "The total time for the Parallel search is:"
           << myTimer.elapsed() << "milisedonds.";
  qDebug() << "================================";

  delete T1;
  delete T2;
}


int main(int argc, char *argv[]){
  unsigned int c = 0;

  srand(time(NULL));

  while(c != twoHundredMillion){
      array[c] = ((double)rand()/(double)RAND_MAX) * ((double)rand()/(double)RAND_MAX)
               * ((double)rand()/(double)RAND_MAX) * (double)twoHundredMillion;
      //array[c] is an element of [0, 200000000]
    c++;
  }

   for(unsigned short i = 0; i < 5; i++)
   {
    //test00();
    //test01();
    //test02B();
    //testRaceCondition();
     //testMutex();
   }


   maxSequential(array,twoHundredMillion);
   MaxParallel();

   delete [] array;

   return(0);
}

